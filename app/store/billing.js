export const state = () => ({
  allInvoices: [],
  invoiceById: {},
  paymentMethods: [],
  paymentTypes: [],
  currencies: [],
})

export const getters = {
  invoices: (s) => s.allInvoices,
  invoiceById: (s) => s.invoiceById,
  paymentMethods: (s) => s.paymentMethods?.map((obj) => ({ label: obj.value, value: obj.value })),
  paymentTypes: (s) => s.paymentTypes?.map((obj) => ({ label: obj.value, value: obj.value })),
  currencies: (s) => s.currencies?.map((obj) => ({ label: obj.value, value: obj.value })),
}

export const mutations = {
  SET_INVOICES(state, { invoices }) {
    state.allInvoices = invoices.data
  },
  SET_INVOICE_BY_ID(state, { invoice }) {
    state.invoiceById = invoice
  },
  SET_PAYMENT_METHODS(state, { methods }) {
    state.paymentMethods = methods.data
  },
  SET_PAYMENT_TYPES(state, { types }) {
    state.paymentTypes = types.data
  },
  SET_CURRENCIES(state, { currencies }) {
    state.currencies = currencies.data
  },
}

export const actions = {
  async fetchInvoices({ commit }) {
    const { data: invoices } = await this.$axios.get('/api/v2/billing/invoices')
    commit('SET_INVOICES', { invoices })
  },
  async fetchInvoiceById({ commit }, id) {
    const { data: invoice } = await this.$axios.get(`/api/v2/billing/invoices/${id}`)
    commit('SET_INVOICE_BY_ID', { invoice })
  },
  async fetchPaymentMethods({ commit }) {
    const { data: methods } = await this.$axios.get('/api/v2/billing/payment-methods')
    commit('SET_PAYMENT_METHODS', { methods })
  },
  async fetchPaymentTypes({ commit }) {
    const { data: types } = await this.$axios.get('/api/v1/billing/payment-types')
    commit('SET_PAYMENT_TYPES', { types })
  },
  async fetchCurrencies({ commit }) {
    const { data: currencies } = await this.$axios.get('/api/v1/billing/currencies')
    commit('SET_CURRENCIES', { currencies })
  },
  async addInvoice(_, form) {
    try {
      const { data: data } = await this.$axios.post('/api/v2/orders/128/add_invoice', form)
      this.$toast.success(data.message)
    } catch (err) {
      this.$toast.error(err)
    }
  },
}
