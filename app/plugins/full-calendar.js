/* eslint-disable vue/component-definition-name-casing */
import Vue from 'vue'
import Calendar from '@/components/elements/Calendar'

Vue.component('full-calendar', Calendar)
